#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys

from src.cdn import Cdn

argv = sys.argv
cnt = len(argv)
if cnt == 2 and argv[1] == "--help":
    print("Download all updates from cdn.")
    print("Format:")
    print(" ./all.py [include [exclude]]")
    print("Examples:")
    print(" download all except .grf/.bmp/.mp3:")
    print("  ./all.py")
    print(" download all except .mp3:")
    print("  ./all.py \"\" \"[.]mp3\"")
    print(" download all files:")
    print("  ./all.py \"\"")
    print(" download dll files if in name not letters '100':")
    print("  ./all.py \"[.]dll\" \"100\"")
    print(" download all exe files:")
    print("  ./all.py \"[.]exe\"")
    exit(0)
if cnt < 3:
    if cnt == 1:
        exclude = "[.](grf|bmp|mp3)"
    else:
        exclude = ""
else:
    exclude = argv[2]
if cnt < 2:
    mask = ""
else:
    mask = argv[1]

print("Include mask: " + mask)
print("Exclude mask: " + exclude)

cdn = Cdn()
cdn.collectAllKnownVersions()
cdn.downloadFilesFromVersions(mask, exclude)
#cdn.downloadFilesFromVersions("[.](dll|exe|lua|lub|bmp)", "")
print("Found versions: {0}".format(len(cdn.knownVersions)))
