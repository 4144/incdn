#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys

from src.cdn import Cdn

cdn = Cdn()

for m in xrange(5, 9):
    for d in xrange(0, 32):
        for ver in xrange(0, 21):
            m = str(m)
            d = str(d)
            ver = str(ver)
            if len(m) < 2:
                m = "0" + m
            if len(d) < 2:
                d = "0" + d
            if len(ver) < 2:
                cdn.tryAddVersion("2019{0}{1}.{2}".format(m, d, ver))
                ver = "0" + ver
            cdn.tryAddVersion("2019{0}{1}.{2}".format(m, d, ver))
