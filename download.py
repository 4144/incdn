#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys

from src.cdn import Cdn
from src.log import logError

argv = sys.argv
cnt = len(argv)
if cnt < 2 or (cnt == 2 and argv[1] == "--help"):
    print("Download all updates from cdn.")
    print("Format:")
    print(" ./download.py version [include [exclude]]")
    print("Examples:")
    print(" download all from latest version except .grf/.bmp/.mp3:")
    print("  ./download.py latest")
    print(" download all files from version 20180820.02:")
    print("  ./download.py 20180820.02 \"\"")
    print(" download all files except .mp3 from version 20180820.02:")
    print("  ./download.py 20180820.02 \"\" \"[.]mp3\"")
    print(" download dll files if in name not letters '100' from latest version:")
    print("  ./download.py latest \"[.]dll\" \"100\"")
    print(" download all exe files from version 20180820.02:")
    print("  ./download.py 20180820.02 \"[.]exe\"")
    exit(0)
if cnt < 4:
    if cnt == 2:
        exclude = "[.](grf|bmp|mp3)"
    else:
        exclude = ""
else:
    exclude = argv[3]
if cnt < 3:
    mask = ""
else:
    mask = argv[2]
version = argv[1]

print("Include mask: " + mask)
print("Exclude mask: " + exclude)

cdn = Cdn()
if version == "latest":
    cdn.downloadLatestVersionNumber()
    version = cdn.version
elif cdn.tryAddVersion(version) is False:
    logError("version {0} not found.".format(version))
    exit(1)

cdn.downloadFilesFromVersion(version, mask, exclude)
