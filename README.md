Downloader for ragnarok updates from one russian CDN.

Allow download updates for ruRo and euRo.

For download all updates except (*.grf and *.mp3) run ``./all.py``

For download all updates with all files run ``./all.py ""``

For download one version except (*.grf and *.mp3) run ``./download.py 20180820.02``

For download one version with all files run ``./download.py 20180820.02 ""``

For list all versionse run ``./list.py``

For get more info about flags, run each script with parameter ``--help``

For working this tool need command line program ``xz``
