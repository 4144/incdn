#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from src.downloader import downloadFile
from xml.dom import minidom
from xml.etree import ElementTree

from src.log import logError

def parseConfig(url, key):
    xml = minidom.parseString(downloadFile(url))
    for node in xml.getElementsByTagName('game'):
        if node.attributes["key"].value == key:
            return (node.attributes["url"].value,
                    node.attributes["base_url"].value)
    logError("Error: key {0} not found".format(key))
    exit(1)
