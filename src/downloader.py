#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gc
import os

from src.log import logDebug

try:
    # For Python 3.0 and later
    from urllib.request import urlopen
except ImportError:
    # Fall back to Python 2's urllib2
    from urllib2 import urlopen

def downloadFile(url):
    logDebug("download {0}".format(url))
    response = urlopen(url)
    return response.read()

def downloadSaveFileRet(url, fileName):
    if os.path.exists(fileName) is True:
        logDebug("skip download {0}".format(url))
        with open(fileName, "rb") as r:
            return r.read()
    logDebug("download {0}".format(url))
    response = urlopen(url)
    data = response.read()
    logDebug("save {0}".format(fileName))
    with open(fileName, "wb") as w:
        w.write(data)
    return data

def downloadSaveFile(url, fileName):
    if os.path.exists(fileName) is True:
        return
    logDebug("download {0}".format(url))
    response = urlopen(url)
    data = response.read()
    logDebug("save {0}".format(fileName))
    with open(fileName, "wb") as w:
        w.write(data)
    data = None
    gc.collect()
    return
