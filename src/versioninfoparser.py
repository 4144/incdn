#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
import os


lineSplit = re.compile(b"\n")
eqSplit = re.compile(b"=")

class VersionInfoParser:
    def __init__(self):
        self.lines = None
        self.num = 0
        self.sz = 0
        # version = path
        self.versions = dict()

    def skipUntil(self, data):
        while self.num < self.sz:
            line = self.lines[self.num]
            self.num = self.num + 1
            if line.find(data) == 0:
                return

    def parseBlock(self):
        blockVars = dict()
        while self.num < self.sz:
            line = self.lines[self.num]
            if line[0] != " ":
                break
            parts = eqSplit.split(line)
            blockVars[parts[0].strip()] = parts[1][:-1].strip()
            self.num = self.num + 1
        return blockVars


    def parseGroup(self, name):
        self.skipUntil(name)
        while self.num < self.sz:
            block = self.parseBlock()
            if len(block) > 0:
                files = block["files"]
                if files.find("../delta/") == 0:
                    files = files[len("../delta/"):-1]
                if files == "./":
                    continue
                self.versions[block["version"]] = files
            line = self.lines[self.num]
            if line[0] != "\r":
                break
            self.num = self.num + 1


    def parse(self, data):
        lines  = lineSplit.split(data)
        self.lines = lines
        self.num = 0
        self.sz = len(lines)
        self.parseGroup(b"distribution ")
        self.parseGroup(b"delta ")
