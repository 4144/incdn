#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
import os

from src.log import logDebug, logError


lineSplit = re.compile("\n")
cntSplit = re.compile("[|]")

class FileInfo:
    def __init__(self):
        self.compressed = None
        self.fileName = None
        self.checkSum = None
        self.pack = None
        self.start = None
        self.size = None
        self.realSize = None


class ControlParser:
    def __init__(self):
        # files ready for download
        self.files = dict()
        # files detected downloaded already
        self.localFiles = dict()
        self.packs = set()

    def parse(self, data, savePath, mask, exclude):
        maskre = re.compile(mask, re.IGNORECASE)
        excludere = re.compile(exclude, re.IGNORECASE)
        lines  = lineSplit.split(data)
        for line in lines:
            parts = cntSplit.split(line)
            if len(parts) < 8:
                continue
            if mask != "":
                m = maskre.search(parts[1])
                if m is None:
                    logDebug("exclude by mask: {0}".format(parts[1]))
                    continue
            if exclude != "":
                m = excludere.search(parts[1])
                if m is not None:
                    logDebug("exclude by mask: {0}".format(parts[1]))
                    continue
            fi = FileInfo()
            fi.compressed = parts[0]
            fi.fileName = parts[1].replace("\\", "/")
            fi.checkSum = parts[2]
            fi.pack = parts[4]
            fi.start = int(parts[5])
            fi.end = int(parts[6])
            if fi.compressed != "0":
                fi.realSize = int(parts[7])
            else:
                fi.realSize = fi.end - fi.start
            if parts[3] != "":
                logError("Found unknown field in control file")
            if os.path.exists(savePath + fi.fileName) is True:
                logDebug("skip add file: {0}".format(fi.fileName))
                self.localFiles[fi.fileName] = fi
                continue
            self.files[fi.fileName] = fi
            self.packs.add(fi.pack)
