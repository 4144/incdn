#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gc
import re
import os
import urllib2
import shutil

from src.configparser import parseConfig
from src.controlparser import ControlParser
from src.downloader import downloadSaveFile, downloadSaveFileRet
from src.log import logDebug, logError
from src.versioninfoparser import VersionInfoParser

dotSplit = re.compile("[.]")


class Cdn:
    def __init__(self):
        self.gameName = os.path.basename(os.path.abspath("."))
        self.latestUrl = None
        self.version = None
        if self.gameName == "roeu":
#            self.baseUrl = "http://rcdn.inn.ru/roeu/patch/restricted/"
            self.baseUrl = "http://cdn.inn.ru/roeu/patch/"
        elif self.gameName == "rot-ru":
            self.baseUrl = "http://cdn.inn.ru/rot/patch/"
        elif self.gameName == "ropeu":
            self.baseUrl = "http://cdn.inn.ru/roteu/patch/"
        else:
            self.baseUrl = "http://cdn.inn.ru/ragnarok/patch/"
        self.root = "updates_{0}/".format(self.gameName)
        self.savePath = self.root
        self.versionInfo = None
        self.knownVersions = dict()
        # files ready for download
        self.files = None
        # already downloaded files
        self.localFiles = dict()
        self.packs = None
        self.control = None

    def setPath(self, path):
        if path is None:
            path = ""
        self.savePath = self.root + path
        if self.savePath[-1] != "/":
            self.savePath = self.savePath + "/"
        if not os.path.exists(self.savePath):
            os.makedirs(self.savePath)
        logDebug("savePath: {0}".format(self.savePath))

    def downloadLatestVersionNumber(self):
        self.downloadConfig("http://cdn.inn.ru/4game/config-live.xml",
                            self.gameName)

    def selectVersion(self, version):
        self.version = version
        self.setPath(version)

    def downloadConfig(self, url, key):
        self.latestUrl, self.baseUrl = parseConfig(url, key)
        latestVersion = self.latestUrl
        if latestVersion.find(self.baseUrl) != 0:
            logError("base url not in latest url")
            exit(1)
        latestVersion = latestVersion[len(self.baseUrl):]
        idx = latestVersion.find("/distr/live")
        sz = len("/distr/live")
        if idx + sz != len(latestVersion):
            logError("/dist/line not found in latest url")
            exit(1)
        self.latestVersion = latestVersion[:idx]
        logDebug("Latest version: {0}".format(self.latestVersion))
        self.selectVersion(self.latestVersion)

    def downloadIndexRet(self, name):
        return downloadSaveFileRet(self.baseUrl + self.version + "/distr/" + name,
                                   self.savePath + name)

    def downloadIndex(self, name):
        downloadSaveFile(self.baseUrl + self.version + "/distr/" + name,
                         self.savePath + name)

    def downloadVersionInfo(self):
        try:
            self.versionInfo = self.downloadIndexRet("version-info.txt")
        except urllib2.HTTPError:
            logError("download error")
            self.versionInfo = None

    def parseVersionInfo(self):
        version = VersionInfoParser()
        version.parse(self.versionInfo)
        for key in version.versions:
            if key not in self.knownVersions:
                name = version.versions[key]
                self.knownVersions[key] = name
                logDebug("add version {0}".format(name))
        if self.version not in self.knownVersions:
            self.knownVersions[self.version] = self.version
            logDebug("add version {0}".format(self.version))

    def downloadControl(self):
        self.control = self.downloadIndexRet("control.txt")

    def downloadDelete(self):
        self.delete = self.downloadIndexRet("delete.txt")

    def downloadPacks(self):
        for pack in sorted(self.packs):
            self.downloadIndex(pack)

    def deletePacks(self):
        for pack in sorted(self.packs):
            logDebug("remove pack {0}".format(pack))
            os.remove(self.savePath + pack)

    def extractFiles(self, skipUnpacking):
        for key in sorted(self.files.keys()):
            fi = self.files[key]
            fileName = self.savePath + "files/" + fi.fileName
            fileName = fileName.replace("\"", "_")
            if skipUnpacking is True and os.path.exists(fileName) is True:
                logDebug("skip unpack file: {0}".format(fi.fileName))
                continue
            dirName = os.path.dirname(fileName)
            if dirName.find("..") >= 0:
                logError("found malformed path: {0}".format(dirName))
                dirName = dirName.replace("..", "__")
            if os.path.exists(dirName) is False:
                os.makedirs(dirName)
            with open(self.savePath + fi.pack, "rb") as r:
                r.seek(fi.start)
                data = r.read(fi.end - fi.start)
            if fi.compressed == "1":
                with open(fileName + ".xz", "wb") as w:
                    w.write(data)
                data = None
                gc.collect()
                cmd = "xz -dc <\"{0}\" >\"{1}\"".format(fileName + ".xz", fileName)
                logDebug("cmd: {0}".format(cmd))
                os.system(cmd)
                os.remove(fileName + ".xz")
                if os.path.exists(fileName) is False:
                    logError("Decompressed file not found: {0}".format(fileName))
                    exit(1)
                if os.path.getsize(fileName) != fi.realSize:
                    logError("wrong size after decompress {0} but should be {1}".format(len(data), fi.realSize))
                    exit(1)
                self.localFiles[key] = fi
            elif fi.compressed == "0":
                with open(fileName, "wb") as w:
                    w.write(data)
                data = None
                gc.collect()
                self.localFiles[key] = fi
            else:
                logError("unknown compression {0} for file {1}".format(fi.compressed, fi.fileName))
                exit(1)

    def tryAddVersion(self, version):
        oldVersion = self.version
        self.selectVersion(version)
        try:
            self.downloadVersionInfo()
            if self.versionInfo is not None:
                self.parseVersionInfo()
                logDebug("detected version {0}".format(version))
        except urllib2.HTTPError:
            self.selectVersion(oldVersion)
            return False
        self.selectVersion(oldVersion)
        return True

    def findMissingSubVersions(self):
        versions = dict()
        for version in sorted(self.knownVersions):
            parts = dotSplit.split(version)
            if len(parts) == 2:
                ver = versions.get(parts[0], set())
                ver.add(parts[1])
                versions[parts[0]] = ver
        for version in versions:
            vals = versions[version]
            n1 = min(vals)
            n2 = max(vals)
            for val in xrange(int(n1), int(n2)):
                val = str(val)
                while len(val) < 2:
                    val = "0" + val
                if val not in vals:
                    logDebug("check missing version: {0}.{1}".format(version, val))
                    self.tryAddVersion(version + "." + val)

    def parseControl(self, mask, exclude):
        control = ControlParser()
        control.parse(self.control, self.savePath + "files/", mask, exclude)
        self.files = control.files
        self.localFiles = control.localFiles
        self.packs = control.packs

    def addFixedVersions(self):
        # known existing versions
        if self.gameName == "roeu" or self.gameName == "rot-ru" or self.gameName == "ropeu":
            pass
        else:
            self.tryAddVersion("20180716.01")
            self.tryAddVersion("20180716.02")
            self.tryAddVersion("20180730.01")
            self.tryAddVersion("20180730.11")
            self.tryAddVersion("20180730.12")
            self.tryAddVersion("20180803.01")
            self.tryAddVersion("20180803.02")
            self.tryAddVersion("20180808.01")
            self.tryAddVersion("20180809.01")
            self.tryAddVersion("20180809.02")
            self.tryAddVersion("20180810.01")
            self.tryAddVersion("20180813.01")
            self.tryAddVersion("20180814.01")
            self.tryAddVersion("20180814.02")
            self.tryAddVersion("20180815.01")
            self.tryAddVersion("20180815.02")
            self.tryAddVersion("20180815.03")
            self.tryAddVersion("20180815.07")
            self.tryAddVersion("20180816.01")
            self.tryAddVersion("20180820.01")
            self.tryAddVersion("20180820.02")
            self.tryAddVersion("20180822.01")
            self.tryAddVersion("20180822.02")
            self.tryAddVersion("20180903.01")
            self.tryAddVersion("20180904.01")
            self.tryAddVersion("20180905.01")
            self.tryAddVersion("20180905.02")
            self.tryAddVersion("20180906.01")
            self.tryAddVersion("20180911.00")
            self.tryAddVersion("20180918.00")
            self.tryAddVersion("20180920.00")
            self.tryAddVersion("20180924.00")
            self.tryAddVersion("20180925.01")
            self.tryAddVersion("20181001.00")
            self.tryAddVersion("20181001.01")
            self.tryAddVersion("20181002.00")
            self.tryAddVersion("20181002.01")
            self.tryAddVersion("20181009.00")
            self.tryAddVersion("20181009.01")
            self.tryAddVersion("20181016.01")
            self.tryAddVersion("20181023.01")
            self.tryAddVersion("20181023.04")


    def collectAllKnownVersions(self):
        self.downloadLatestVersionNumber()
        self.downloadVersionInfo()
        self.parseVersionInfo()
        self.addFixedVersions()
        for version in sorted(self.knownVersions):
            self.selectVersion(version)
            self.downloadVersionInfo()
            if self.versionInfo is None:
                continue
            self.parseVersionInfo()

    def downloadFilesFromVersion(self, version, mask, exclude):
        self.selectVersion(version)
        self.downloadVersionInfo()
        if self.versionInfo is None:
            return
        self.downloadControl()
        self.parseControl(mask, exclude)
        self.downloadPacks()
        self.extractFiles(True)
        self.downloadDelete()
        self.deletePacks()

    def downloadFilesFromVersions(self, mask, exclude):
        for version in sorted(self.knownVersions):
            self.selectVersion(version)
            self.downloadVersionInfo()
            if self.versionInfo is None:
                continue
            self.downloadControl()
            self.parseControl(mask, exclude)
            self.downloadPacks()
            self.extractFiles(True)
            self.downloadDelete()
            self.deletePacks()

    def extractClients(self):
        for version in sorted(self.knownVersions):
            self.selectVersion(version)
            self.downloadVersionInfo()
            if self.versionInfo is None:
                continue
            self.downloadControl()
            self.parseControl("Ragexe[.]exe", "")
            self.downloadPacks()
            self.extractFiles(True)
            self.downloadDelete()
            self.deletePacks()
            self.copyFilesFromVersion("clients/")

    def copyFilesFromVersion(self, dirName):
        for name in self.localFiles:
            dstName = "{0}{1}{2}".format(dirName, self.version, name)
            logDebug("extract file {0}".format(dstName))
            shutil.copyfile("{0}files/{1}".format(self.savePath, name),
                            dstName)
